﻿using UnityEngine;
using System.Collections;

public class PlaneStuff : MonoBehaviour
{
    public int cameraNum = 0; // Zero based
    private Camera cam;
    public AudioSource motorAudio;
    private Transform trans;
    private Rigidbody rb;

    Vector3 prevPos = new Vector3();
    Vector3 prevVel = new Vector3();
    private float calculLerped;
    public float soundLerpSpeed = 0.6f;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        cam = GetComponentInChildren<Camera>();
        trans = transform;

        prevPos = trans.position;

        var totalOfPlanes = FindObjectsOfType<PlaneStuff>().Length;
        var w = 1 / totalOfPlanes;
        this.cam.rect = new Rect(w * cameraNum, 0, w, 1);
    }

    void Reset()
    {
        Awake();
    }

    void FixedUpdate()
    {
        var vel = trans.position - prevPos;
        var acc = vel - prevVel; // Acceleration

        var mag = vel.magnitude;
        var calcul = mag / 7; // normalized
        calcul = Mathf.Sqrt(calcul); // get values closer to 1
        calcul *= 2.5f; // 3 is the maxinum good sounding pitch
        calculLerped += (calcul - calculLerped) * soundLerpSpeed;

        motorAudio.pitch = calcul;


        prevVel = vel;
        prevPos = trans.position;
    }

    public void Die()
    {
        StartCoroutine(DieSequence());
    }

    private IEnumerator DieSequence()
    {
        Debug.Log("This plane died.");
        yield return new WaitForSeconds(0.5f);
        // Explode
        yield return new WaitForSeconds(0.5f);
        Debug.Log("TODO: Show fullscreen window");
    }
}
