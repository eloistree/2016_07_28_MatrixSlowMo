﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

public class GunController : MonoBehaviour {

	public float _ReloadTime;
	public int _MagazineSize = 15;

	private bool _ReadyToFire = true;
	private bool _Recharging = false;
	private int _Bullets = 6;

	private AudioSource _Empty;

	//public WandController _Controller;
	public Transform _GunTip;
	public GameObject _BulletPrefab;


    [Header("Events")]
    public UnityEvent _onBulletFiredEvent;

	// Use this for initialization
	void Start () {
		_Empty = GetComponent<AudioSource> ();
		_Bullets = _MagazineSize;
	}

    public enum HandDirection {Left, Right }
    public HandDirection m_direction;
	// Update is called once per frame
	void Update () {
		if (IsTriggerDown(m_direction))
		{
			if (_ReadyToFire && _Bullets > 0)
			{
				_Bullets--;
				FireBullet ();
			} else
			{
				StartCoroutine (EmptyRumble ());
				_Empty.Play ();
				_ReadyToFire = false;

				if (!_Recharging)
				{
					_Recharging = true;
					StartCoroutine (Recharge ());
				}
			}
		}
        if (m_countToStop > 0f) {
            m_countToStop -= Time.deltaTime;
            if (m_countToStop <= 0f) {
                StopVibration(m_direction);
            }
        }
	}
    private bool IsTriggerDown(HandDirection direction)
    {
        OVRInput.Controller controller = direction == HandDirection.Left ? OVRInput.Controller.LTouch : OVRInput.Controller.RTouch;
        return OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger , controller);
    }
    private void Vibrate(HandDirection direction, int ms)
    {
        OVRInput.Controller controller = direction == HandDirection.Left ? OVRInput.Controller.LTouch : OVRInput.Controller.RTouch;
        OVRInput.SetControllerVibration(.3f, 0.3f, controller);
        m_countToStop = ((float)ms) / 1000;
    }
    private void StopVibration(HandDirection direction)
    {
        OVRInput.Controller controller = direction == HandDirection.Left ? OVRInput.Controller.LTouch : OVRInput.Controller.RTouch;
        OVRInput.SetControllerVibration(0, 0, controller);
        m_countToStop = 0;

    }
    public float m_countToStop;


    private void FireBullet(){
		//instantiate bullet
		GameObject createdBullet = Instantiate (_BulletPrefab, _GunTip.position, Quaternion.identity) as GameObject;
		createdBullet.transform.forward = _GunTip.transform.forward;
        _onBulletFiredEvent.Invoke();
        //vibrate controller
		StartCoroutine(FireRumble(10, 0.075f*TimeSlower.factor));

	}
		
	IEnumerator Recharge(){
		Debug.Log (_ReloadTime * TimeSlower.factor);
		yield return new WaitForSeconds (_ReloadTime*TimeSlower.factor);

		StartCoroutine (RechargeRumble ());
		_Bullets = _MagazineSize;
		_ReadyToFire = true;
		_Recharging = false;
	}

	IEnumerator RechargeRumble()
    {
        Vibrate( m_direction, 2000);
        yield return new WaitForSeconds(0.05f * TimeSlower.factor);
        Vibrate(m_direction, 2000);
    }

  
    IEnumerator EmptyRumble()
    {
        Vibrate(m_direction, 2000);
        yield return new WaitForSeconds (0.05f*TimeSlower.factor);
	}

	IEnumerator FireRumble(int rumbles, float duration){
		float timePassed = 0;

		while (timePassed < duration)
		{
			timePassed += ( duration / rumbles );

            Vibrate(m_direction, 2000);
            yield return new WaitForSeconds ((duration / rumbles)*TimeSlower.factor);
		}
	}

    

}
