﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour
{
    public bool m_isActive;
    public Rigidbody m_targetRig;
    public Transform m_startPoint;
    public Transform m_directionPoint;
    public float m_forceBySecond=5;
    public float m_enterImpulsion = 3;


    public void Update()
    {
        if (!m_isActive) return;
        m_targetRig.AddForce((m_directionPoint.position - m_startPoint.position).normalized * m_forceBySecond * Time.deltaTime, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        m_startPoint = transform;
        m_directionPoint = other.gameObject.transform;
        m_targetRig = other.attachedRigidbody;

        m_targetRig.AddForce((m_directionPoint.position - m_startPoint.position).normalized * m_enterImpulsion , ForceMode.Impulse);
        m_isActive = true; 
    }

    private void OnTriggerExit(Collider other)
    {
        m_isActive = false;
    }
}
