﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameController : MonoBehaviour {

    
	public static int currentScore = 0;
	public  float delayToRestart= 5f;
    public float playSound = 2;
    public AudioSource m_reloadScene;
    public string m_reloadSceneName = "";
    public UnityEvent m_onReloadScene;
    

	public void RestartWithDelay(){

        Invoke("RestartSound", playSound);

        Invoke("Restart", delayToRestart);

    }
    void RestartSound()
    {
        m_reloadScene.Play();
    }
        void Restart()
        {
        m_onReloadScene.Invoke();
        //SceneManager.LoadScene(m_reloadSceneName);
    }
	// Update is called once per frame
	private IEnumerator GameLoop(){
		yield return StartCoroutine(RoundStarting());
		yield return StartCoroutine(RoundPlaying());
		yield return StartCoroutine(RoundEnding());
	}

	private IEnumerator RoundStarting(){
		yield return new WaitForSeconds (1f);
	}

	private IEnumerator RoundPlaying(){
		yield return new WaitForSeconds (1f);
	}

	private IEnumerator RoundEnding(){
		yield return new WaitForSeconds (1f);
	}
}
