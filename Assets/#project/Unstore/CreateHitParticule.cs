﻿using UnityEngine;
using System.Collections;

public class CreateHitParticule : MonoBehaviour {

    public Transform _where;
    public GameObject _prefabCreated;
    public float _lifeTime=5f;

    public void CreatePrefabAtCurrentPosition () {

        GameObject prefab = GameObject.Instantiate(_prefabCreated, _where.position , _where.rotation) as GameObject;
        Destroy(prefab, _lifeTime);
	}

    void Reset() {
        if (_where == null) {
            _where = transform;
        }
    }
}
