﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoLoadNextScene : MonoBehaviour
{
    public string m_sceneName;
    public float m_timeToLoad= 1;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(m_timeToLoad);
        if(string.IsNullOrEmpty(m_sceneName))
         SceneManager.LoadScene(   SceneManager.GetActiveScene().buildIndex);
        else
            SceneManager.LoadScene(m_sceneName);
    }
    
}
