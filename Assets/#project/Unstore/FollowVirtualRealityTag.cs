﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowVirtualRealityTag : MonoBehaviour
{
    public VirtualRealityTag.Type m_trackedType;
    public Transform m_affected;
    [Header("Debug")]
    public VirtualRealityTag m_tracked;
    void Update()
    {
        if (m_tracked == null) {
            m_tracked = VirtualRealityTag.Get(m_trackedType);
        }

        if (m_affected == null && m_tracked == null)
            return;

        m_affected.position = m_tracked.transform.position;
        m_affected.rotation = m_tracked.transform.rotation;
        m_affected.localScale = m_tracked.transform.localScale;

    }

    private void OnValidate()
    {
        if (m_affected == null)
            m_affected = this.transform;
    }
}
