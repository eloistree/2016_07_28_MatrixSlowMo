﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour {
	public static int _bestScoreShooter=9999;
	public static int _bestScoreRunner=0;
    public int _score = 0;
	public Text _textDisplayer;
	public Text _textBestDisplay;
	public Text _textBestRunnerDisplay;
    public GameObject _textRoot;


	public void SaveActualScore(){
		if (_score < _bestScoreShooter)
		{

			_bestScoreShooter = _score;

		}
		if (_score > _bestScoreRunner)
		{

			_bestScoreRunner = _score;

		}
			_textBestDisplay.text = ""+_bestScoreShooter;
			_textBestRunnerDisplay.text = ""+_bestScoreRunner;

	}
    private void Start()
    {
        Refresh();
    }
    public void Refresh(){
        if (_bestScoreShooter == 9999 && _bestScoreRunner == 0)
        {
            _textBestDisplay.text = "-" ;
            _textBestRunnerDisplay.text = "-";

        }
        else {

		_textBestDisplay.text = ""+_bestScoreShooter;
		_textBestRunnerDisplay.text = ""+_bestScoreRunner;
        }
		_textDisplayer.text = ""+_score;

	}

    public void AddScore() {
        _score++;

        Refresh();
    }

    public void ResetScore() {
        _score = 0;
        _textDisplayer.text = "" + 0;

    }

    public void DisplayScore(bool isDisplay)
    {
        _textRoot.SetActive(isDisplay);
    }


    public void FullReset() {
        _bestScoreShooter = 9999;
        _bestScoreRunner = 0;
        _score = 0;
    }
}
