﻿using UnityEngine;
using System.Collections;

public class Restart : MonoBehaviour {

    public float _timeToRestart=1f;


	public void RestartGame () {

        Invoke("RestartTheLevel", _timeToRestart);
	}

     void RestartTheLevel() {
        Application.LoadLevel(Application.loadedLevel);
    }

	
}
