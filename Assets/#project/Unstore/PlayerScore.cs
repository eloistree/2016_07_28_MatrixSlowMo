﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class PlayerScore : MonoBehaviour {


    public int _hasBeenHit;
    public UnityEvent _playerReceivedHit;
	
	public void AddHitCountPoint ()
    {
        _hasBeenHit++;
        _playerReceivedHit.Invoke();
    }
    public void ResetHitCoint (){ _hasBeenHit = 0; }
    
}
