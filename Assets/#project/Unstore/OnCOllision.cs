﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnCOllision : MonoBehaviour
{
    public UnityEvent m_onCollision;

    private void OnCollisionEnter(Collision collision)
    {
        m_onCollision.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {

        m_onCollision.Invoke();
    }
}
