﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluetoothControllerExperiment : MonoBehaviour
{
    public float m_rotationSpeed=90f;

    void Update()
    {
        Vector2 m_axes = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if (m_axes != Vector2.zero) {
            transform.Rotate(new Vector3(m_axes.x, m_axes.y), m_rotationSpeed*Time.deltaTime);
        }
    }
}
