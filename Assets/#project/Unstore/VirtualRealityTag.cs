﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualRealityTag : MonoBehaviour
{
    public enum Type{ Head, LeftHand, RightHand, LeftEye, RightEye, CenterEyes }
    public Type m_tagType;
    public static List<VirtualRealityTag> m_tagsInScene = new List<VirtualRealityTag>();
    public static  Dictionary<Type, VirtualRealityTag> m_tagsByTypeInScene = new Dictionary<Type, VirtualRealityTag>();

    internal static VirtualRealityTag Get(Type type)
    {
        if (m_tagsByTypeInScene.ContainsKey(type))
         return m_tagsByTypeInScene[type] ;
        return null;
    }

    private void OnEnable()
    {
        m_tagsInScene.Add(this);

        if (!m_tagsByTypeInScene.ContainsKey(this.m_tagType))
            m_tagsByTypeInScene.Add(this.m_tagType, this);
        else m_tagsByTypeInScene[this.m_tagType]=this;

    }
    private void OnDisable()
    {
        m_tagsInScene.Remove(this);
        m_tagsByTypeInScene.Remove(this.m_tagType);
    }


}
